/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_can_int_callback.h"

bool CAN_tx_bat_data_Flag = false;
bool OpenDockLatch_Flag = false;
bool Capture_BatData_Flag = false;
bool TurnONCharger_Flag = false;
bool Tx_Ids_Flag = false;    
bool UnlockLatchProcessStart = false;
uint8_t latch_num[4];
/**
  ==============================================================================
                          ##### Callback functions #####
  ==============================================================================
    [..]
    This subsection provides the following callback functions:
      ( ) HAL_CAN_TxMailbox0CompleteCallback
      ( ) HAL_CAN_TxMailbox1CompleteCallback
      ( ) HAL_CAN_TxMailbox2CompleteCallback
      ( ) HAL_CAN_TxMailbox0AbortCallback
      ( ) HAL_CAN_TxMailbox1AbortCallback
      ( ) HAL_CAN_TxMailbox2AbortCallback
      (+) HAL_CAN_RxFifo0MsgPendingCallback
      ( ) HAL_CAN_RxFifo0FullCallback
      ( ) HAL_CAN_RxFifo1MsgPendingCallback
      ( ) HAL_CAN_RxFifo1FullCallback
      ( ) HAL_CAN_SleepCallback
      ( ) HAL_CAN_WakeUpFromRxMsgCallback
      ( ) HAL_CAN_ErrorCallback
  ==============================================================================
*/

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    uint8_t rcvd_msg[8];
    if(hcan->Instance == CAN1)
    {
        if(HAL_CAN_GetRxMessage(&hcan1_t, CAN_RX_FIFO0, &CAN1_RxHeader_t, rcvd_msg) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        if(rcvd_msg[0] == 0x02 && CAN1_RxHeader_t.ExtId >= 0x9C4)
        {
            RE_TurnOff_Location();
            Bat[bat_num].CanId = CAN1_RxHeader_t.ExtId;
            memcpy(Bat[bat_num].PhyId, rcvd_msg + 1, 7);
            Tx_Ids_Flag = true;
            Bat_Registered_Flag = true;
        }
        else
        {
            Bat_Data_Rx_Flag = false;
            memcpy(BatData_Buffer + BataData_Len, rcvd_msg + 1, 6);
            BataData_Len = BataData_Len + 6;
        }
    }
    else
    {
        if(HAL_CAN_GetRxMessage(&hcan2_t, CAN_RX_FIFO0, &CAN2_RxHeader_t, rcvd_msg) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        if(rcvd_msg[0] == 0x09 && CAN2_RxHeader_t.ExtId == 0x1)
        {
            RE_Transmit_SelfIDs();
        }
        else if(rcvd_msg[0] == 0x5F && CAN2_RxHeader_t.ExtId == 0x1)
        {
            uint32_t CanID = 0;
            CanID = (rcvd_msg[1]) | ((rcvd_msg[2]) << 8) | ((rcvd_msg[3]) << 16) | ((rcvd_msg[4]) << 24);
            if(CanID == SelfCanID)
            {
                CAN_tx_bat_data_Flag = true;
            }
        }
        else if(rcvd_msg[0] == 0x5E && CAN2_RxHeader_t.ExtId == 0x1)
        {
            Capture_BatData_Flag = true;
        }
        else if(CAN2_RxHeader_t.ExtId == 0x2)
        {
            uint8_t i = 0;
            if(rcvd_msg[0] == Bat[0].Dock_Location || rcvd_msg[1] == Bat[0].Dock_Location || rcvd_msg[2] ==\
                Bat[0].Dock_Location || rcvd_msg[3] == Bat[0].Dock_Location)
            {
                latch_num[i] = 1;
                i++;
                Unlocklatch1 = true;
            }
            if(rcvd_msg[0] == Bat[1].Dock_Location || rcvd_msg[1] == Bat[1].Dock_Location || rcvd_msg[2] ==\
                Bat[1].Dock_Location || rcvd_msg[3] == Bat[1].Dock_Location)
            {
                latch_num[i] = 2;
                i++;
                Unlocklatch2 = true;
            }
            if(rcvd_msg[0] == Bat[2].Dock_Location || rcvd_msg[1] == Bat[2].Dock_Location || rcvd_msg[2] ==\
                Bat[2].Dock_Location || rcvd_msg[3] == Bat[2].Dock_Location)
            {
                latch_num[i] = 3;
                i++;
                Unlocklatch3 = true;
            }
            if(rcvd_msg[0] == Bat[3].Dock_Location || rcvd_msg[1] == Bat[3].Dock_Location || rcvd_msg[2] ==\
                Bat[3].Dock_Location || rcvd_msg[3] == Bat[3].Dock_Location)
            {
                latch_num[i] = 4;
                i++;
                Unlocklatch4 = true;
            }
            if(Unlocklatch1 | Unlocklatch2 | Unlocklatch3 | Unlocklatch4)
            {  
                if(HAL_TIM_Base_Start_IT(&htim3_t) != HAL_OK)
                {
                    RE_Error_Handler(__FILE__, __LINE__);
                }
            }
        }
        else if(CAN2_RxHeader_t.ExtId == 0x3)
        {
            if(rcvd_msg[0] == Bat[0].Dock_Location)
            {
                charger1Status_Flag = true;
                if(rcvd_msg[1] == 2 && rcvd_msg[2] == 1 && rcvd_msg[3] == 1)
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, GPIO_PIN_SET);
                }
                else
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, GPIO_PIN_RESET);
                }
            }
            else if(rcvd_msg[0] == Bat[1].Dock_Location)
            {
                charger2Status_Flag = true;
                if(rcvd_msg[1] == 2 && rcvd_msg[2] == 1 && rcvd_msg[3] == 1)
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_SET);
                }
                else
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_RESET);
                }                
            }
            else if(rcvd_msg[0] == Bat[2].Dock_Location)
            {
                charger3Status_Flag = true;
                if(rcvd_msg[1] == 2 && rcvd_msg[2] == 1 && rcvd_msg[3] == 1)
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, GPIO_PIN_SET);
                }
                else
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, GPIO_PIN_RESET);
                }                
            }
            else if(rcvd_msg[0] == Bat[3].Dock_Location)
            {
                charger4Status_Flag = true;
                if(rcvd_msg[1] == 2 && rcvd_msg[2] == 1 && rcvd_msg[3] == 1)
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_SET);
                }
                else
                {
                    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_RESET);
                }               
            }           
        }
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/
