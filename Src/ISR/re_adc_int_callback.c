/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_adc_init_callback.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_adc_int_callback.h"

/**
 * @brief ADC conversion complete callback
 * this function handles the interupt callbacks of ADC
 * @param hadc1_t ADC handler typedef
 * @retval None
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/