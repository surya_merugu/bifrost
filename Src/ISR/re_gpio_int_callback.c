/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_gpio_int_callback.c
  * Origin Date           :   06/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_gpio_int_callback.h"

bool Req_Bat1_Id_Flag, Req_Bat2_Id_Flag, Req_Bat3_Id_Flag, Req_Bat4_Id_Flag;
bool Latch1_Opened, Latch2_Opened, Latch3_Opened, Latch4_Opened;

/**
 * @brief GPIO External interrupt Callback
 * This function is being called by the HAL layer whenever GPIO interrupt occurs
 * @param  GPIO Pin Number
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    HAL_NVIC_DisableIRQ(EXTI3_IRQn);
    HAL_NVIC_DisableIRQ(EXTI4_IRQn);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    if (GPIO_Pin == GPIO_PIN_1)
    {
        Bat[0].Dock_Status = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
        if(Bat[0].Dock_Status == true)
        {
            Req_Bat1_Id_Flag = true;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET);
        }
        else
        {
            if(Unlocklatch1 != true)
            {
                Latch1_Opened = true;
            }
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);
        }
    }
    else if(GPIO_Pin == GPIO_PIN_3)
    {    
        Bat[2].Dock_Status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3);
        if(Bat[2].Dock_Status == true)
        {
            Req_Bat3_Id_Flag = true;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_RESET);
        }
        else
        {
            if(Unlocklatch3 != true)
            {
                Latch3_Opened = true;
            }
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, GPIO_PIN_SET);
        }
    }
    else if(GPIO_Pin == GPIO_PIN_4)
    {     
        Bat[3].Dock_Status = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4);
        if(Bat[3].Dock_Status == true)
        {
            Req_Bat4_Id_Flag = true;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET);
        }
        else
        {
            if(Unlocklatch4 != true)
            {
                Latch4_Opened = true;
            }
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET);
        }
    }
    else if(GPIO_Pin == GPIO_PIN_6)
    {     
        Bat[1].Dock_Status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
        if(Bat[1].Dock_Status == true)
        {
            Req_Bat2_Id_Flag = true;
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);
        }
        else
        {
            if(Unlocklatch2 != true)
            {
                Latch2_Opened = true;
            }
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET);
        }
    }
    if(Bat[0].Dock_Status && Bat[1].Dock_Status && Bat[2].Dock_Status && Bat[3].Dock_Status)
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
    }
    else
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
    }
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}


/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/