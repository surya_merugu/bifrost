/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   stm32f4xx_it.c
  * Origin Date           :   25/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "stm32f4xx_it.h"

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{

}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{

}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{

}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{

}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
    HAL_IncTick();
}

/**
  * @brief This function handles CAN1 TX interrupts.
  */
void CAN1_TX_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @brief This function handles CAN1 RX0 interrupts.
  */
void CAN1_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @brief This function handles CAN1 RX1 interrupt.
  */
void CAN1_RX1_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @brief This function handles CAN1 SCE interrupt.
  */
void CAN1_SCE_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @brief This function handles CAN1 TX interrupts.
  */
void CAN2_TX_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @brief This function handles CAN1 RX0 interrupts.
  */
void CAN2_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @brief This function handles CAN1 RX1 interrupt.
  */
void CAN2_RX1_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @brief This function handles CAN1 SCE interrupt.
  */
void CAN2_SCE_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart2_t);
}

/**
  * @brief This function handles ADC1, ADC2 and ADC3 global interrupts.
  */
void ADC_IRQHandler(void)
{
  HAL_ADC_IRQHandler(&hadc1_t);
}

/**
 * @brief This function handles Timer interrupts
 */
void TIM2_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim2_t);
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM5_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim5_t);
}

/**
 * @brief This function handles Timer interrupts
 */
void TIM3_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim3_t);
}

/**
 * @brief This function handles GPIO Pin1 interrupts
 */
void EXTI1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

/**
 * @brief This function handles GPIO Pin2 interrupts
 */
void EXTI3_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

/**
 * @brief This function handles GPIO Pin4 interrupts
 */
void EXTI4_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}

/**
 * @brief This function handles GPIO Pin(5-9) interrupts
 */
void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/