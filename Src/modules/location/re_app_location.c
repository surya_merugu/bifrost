/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_location.c
  * Origin Date           :   24/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/
#include "location/re_app_location.h" 

RE_StatusTypeDef RE_TurnOff_Location(void)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_RESET);
    return RE_OK;
}

RE_StatusTypeDef RE_TurnOn_Dock1Location(void)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
    return RE_OK;
}

RE_StatusTypeDef RE_TurnOn_Dock2Location(void)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
    return RE_OK;
}

RE_StatusTypeDef RE_TurnOn_Dock3Location(void)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
    return RE_OK;
}

RE_StatusTypeDef RE_TurnOn_Dock4Location(void)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET);
    return RE_OK;
}

RE_StatusTypeDef RE_RequestPackId(uint8_t Location)
{
    Bat_Registered_Flag = false;
    switch(Location)
    {
        case 1:
            RE_TurnOff_Location();
            RE_TurnOn_Dock1Location();
            bat_num = 0;
            RE_Req_BatIds();
            break;
        case 2:
            RE_TurnOff_Location();
            RE_TurnOn_Dock2Location();
            bat_num = 1;
            RE_Req_BatIds();
            break;
        case 3:
            RE_TurnOff_Location();
            RE_TurnOn_Dock3Location();
            bat_num = 2;
            RE_Req_BatIds();
            break;
        case 4:
            RE_TurnOff_Location();
            RE_TurnOn_Dock4Location();
            bat_num = 3;
            RE_Req_BatIds();
            break;
        default:
            break;
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/