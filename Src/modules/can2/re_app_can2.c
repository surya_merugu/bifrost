/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_can2.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */
/* Includes*/

#include "can2/re_app_can2.h"

uint8_t bat_num = 0;
uint8_t bat_data_buffer[8];

RE_StatusTypeDef RE_Transmit_SelfIDs(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message[8];
    CAN2_TxHeader_t.DLC   = 5;  
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    CAN2_TxHeader_t.ExtId = 0x7CF;
    tx_message[0]         = 0x01;
    tx_message[1]         = (SelfCanID & 0xFF);
    tx_message[2]         = ((SelfCanID >> 8)  & 0xFF);
    tx_message[3]         = ((SelfCanID >> 16) & 0xFF);
    tx_message[4]         = ((SelfCanID >> 24) & 0xFF);
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }   
    memset(tx_message, 0, sizeof(tx_message));
    CAN2_TxHeader_t.DLC   = 8;
    tx_message[0]         = 0x02;
    memcpy(tx_message + 1, SystemConfig.PhyID, 7);   
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }    
    return RE_OK;
}


RE_StatusTypeDef RE_Transmit_BatteryData(void)
{
    uint32_t TxMailbox;
    CAN2_TxHeader_t.DLC   = 6;
    CAN2_TxHeader_t.ExtId = SelfCanID + 4;
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, bat_data_buffer,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }  
    return RE_OK;
}

RE_StatusTypeDef RE_Transmit_StartOfData(void)
{
    uint32_t TxMailbox;
    CAN2_TxHeader_t.DLC   = 8;
    CAN2_TxHeader_t.ExtId = SelfCanID + 3;
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, bat_data_buffer,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }      
    return RE_OK;
}

void dummy_message(void)
{
    uint32_t TxMailbox;
    CAN2_TxHeader_t.DLC   = 8;
    CAN2_TxHeader_t.ExtId = 0xABC;
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, bat_data_buffer,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }        
}

RE_StatusTypeDef RE_Tx_LatchAck(uint8_t len, uint8_t *pData)
{
    uint32_t TxMailbox;
    CAN2_TxHeader_t.DLC   = len;
    CAN2_TxHeader_t.ExtId = SelfCanID + 9;
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, pData,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }        
    return RE_OK;
}

  
RE_StatusTypeDef RE_Transmit_RegistrationData(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message[8];
    CAN2_TxHeader_t.DLC   = 7;  
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    CAN2_TxHeader_t.ExtId = SelfCanID + 10;
    tx_message[0]         = 12;
    tx_message[1]         = Bat[bat_num].Dock_Location;
    tx_message[2]         = Bat[bat_num].Dock_Status + 1;
    tx_message[3]         = (Bat[bat_num].CanId & 0xFF);
    tx_message[4]         = ((Bat[bat_num].CanId >> 8)  & 0xFF);
    tx_message[5]         = ((Bat[bat_num].CanId >> 16) & 0xFF);
    tx_message[6]         = ((Bat[bat_num].CanId >> 24) & 0xFF);
    switch(bat_num)
    {
        case 0:
            SystemConfig.Bat0_CanId[0]         = ((Bat[bat_num].CanId >> 24) & 0xFF);
            SystemConfig.Bat0_CanId[1]         = ((Bat[bat_num].CanId >> 16)  & 0xFF);
            SystemConfig.Bat0_CanId[2]         = ((Bat[bat_num].CanId >> 8) & 0xFF);
            SystemConfig.Bat0_CanId[3]         = (Bat[bat_num].CanId & 0xFF);
            RE_Write_DwordToNVS(18, SystemConfig.Bat0_CanId);
            RE_Read_DwordFromNVS(18, 0);
            break;
        case 1:
            SystemConfig.Bat1_CanId[0]         = ((Bat[bat_num].CanId >> 24) & 0xFF);
            SystemConfig.Bat1_CanId[1]         = ((Bat[bat_num].CanId >> 16)  & 0xFF);
            SystemConfig.Bat1_CanId[2]         = ((Bat[bat_num].CanId >> 8) & 0xFF);
            SystemConfig.Bat1_CanId[3]         = (Bat[bat_num].CanId & 0xFF);
            RE_Write_DwordToNVS(22, SystemConfig.Bat1_CanId);
            RE_Read_DwordFromNVS(22, 1);
            break;
        case 2:
            SystemConfig.Bat2_CanId[0]         = ((Bat[bat_num].CanId >> 24) & 0xFF);
            SystemConfig.Bat2_CanId[1]         = ((Bat[bat_num].CanId >> 16)  & 0xFF);
            SystemConfig.Bat2_CanId[2]         = ((Bat[bat_num].CanId >> 8) & 0xFF);
            SystemConfig.Bat2_CanId[3]         = (Bat[bat_num].CanId & 0xFF);
            RE_Write_DwordToNVS(26, SystemConfig.Bat2_CanId);
            RE_Read_DwordFromNVS(26, 2);
            break;
        case 3:
            SystemConfig.Bat3_CanId[0]         = ((Bat[bat_num].CanId >> 24)& 0xFF);
            SystemConfig.Bat3_CanId[1]         = ((Bat[bat_num].CanId >> 16)  & 0xFF);
            SystemConfig.Bat3_CanId[2]         = ((Bat[bat_num].CanId >> 8) & 0xFF);
            SystemConfig.Bat3_CanId[3]         = (Bat[bat_num].CanId & 0xFF);
            RE_Write_DwordToNVS(32, SystemConfig.Bat3_CanId);
            RE_Read_DwordFromNVS(32, 3);
            break;
        default:
            break;
    }
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }   
    memset(tx_message, 0, sizeof(tx_message));
    CAN2_TxHeader_t.DLC   = 8;
    tx_message[0]         = 11;
    memcpy(tx_message + 1, Bat[bat_num].PhyId, 7);       
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }   
    return RE_OK;    
}

RE_StatusTypeDef RE_Tx_Bat_Not_Registered(uint8_t bat_num)
{
    uint32_t TxMailbox;
    uint8_t tx_message[8];
    CAN2_TxHeader_t.DLC   = 7;  
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    CAN2_TxHeader_t.ExtId = SelfCanID + 10;
    tx_message[0]         = 12;
    tx_message[1]         = Bat[bat_num].Dock_Location;
    tx_message[2]         = Bat[bat_num].Dock_Status + 1;
    tx_message[3]         = 0;
    tx_message[4]         = 0;
    tx_message[5]         = 0;
    tx_message[6]         = 0;
    switch(bat_num)
    {
        case 0:
            SystemConfig.Bat0_CanId[0]         = 0;
            SystemConfig.Bat0_CanId[1]         = 0;
            SystemConfig.Bat0_CanId[2]         = 0;
            SystemConfig.Bat0_CanId[3]         = 0;
            RE_Write_DwordToNVS(18, SystemConfig.Bat0_CanId);
            RE_Read_DwordFromNVS(18, 0);
            break;
        case 1:
            SystemConfig.Bat1_CanId[0]         = 0;
            SystemConfig.Bat1_CanId[1]         = 0;
            SystemConfig.Bat1_CanId[2]         = 0;
            SystemConfig.Bat1_CanId[3]         = 0;
            RE_Write_DwordToNVS(22, SystemConfig.Bat1_CanId);
            RE_Read_DwordFromNVS(22, 1);
            break;
        case 2:
            SystemConfig.Bat2_CanId[0]         = 0;
            SystemConfig.Bat2_CanId[1]         = 0;
            SystemConfig.Bat2_CanId[2]         = 0;
            SystemConfig.Bat2_CanId[3]         = 0;
            RE_Write_DwordToNVS(26, SystemConfig.Bat2_CanId);
            RE_Read_DwordFromNVS(26, 2);
            break;
        case 3:
            SystemConfig.Bat3_CanId[0]         = 0;
            SystemConfig.Bat3_CanId[1]         = 0;
            SystemConfig.Bat3_CanId[2]         = 0;
            SystemConfig.Bat3_CanId[3]         = 0;
            RE_Write_DwordToNVS(32, SystemConfig.Bat3_CanId);
            RE_Read_DwordFromNVS(32, 3);
            break;
        default:
            break;
    }
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }  
    Bat[bat_num].CanId = 0; 
    memset(tx_message, 0, sizeof(tx_message));
    CAN2_TxHeader_t.DLC   = 8;
    tx_message[0]         = 11;
    for(uint8_t i = 1; i<=7; i++)
    {
        tx_message[i]  = 0;
    }
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }  
    memset(Bat[bat_num].PhyId, 0, sizeof(Bat[bat_num].PhyId));
    return RE_OK;    
}

RE_StatusTypeDef RE_Tx_Charger_Ack(uint8_t Charger_Location)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    CAN2_TxHeader_t.DLC   = 2;  
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    CAN2_TxHeader_t.ExtId = SelfCanID + 8;
    switch(Charger_Location)
    {
        case 1:
            tx_message[0] = Bat[0].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_5) + 1;
            break;
        case 2:
            tx_message[0] = Bat[1].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_6) + 1;
            break;
        case 3:
            tx_message[0] = Bat[2].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_7) + 1;
            break;
        case 4:
            tx_message[0] = Bat[3].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8) + 1;
            break;
        default:
            break;
    }
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }    
    return RE_OK;
}

RE_StatusTypeDef RE_Tx_Latch_Open_Detected(uint8_t Latch_Number)
{
    uint32_t TxMailbox;
    uint8_t tx_message[2];
    CAN2_TxHeader_t.DLC   = 2;  
    CAN2_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN2_TxHeader_t.RTR   = CAN_RTR_DATA;
    CAN2_TxHeader_t.ExtId = SelfCanID + 7;
    switch(Latch_Number)
    {
        case 1:
            tx_message[0] = Bat[Latch_Number - 1].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1) + 1;
            break;
        case 2:
            tx_message[0] = Bat[Latch_Number - 1].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6) + 1;
            break;
        case 3:
            tx_message[0] = Bat[Latch_Number - 1].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3) + 1;
            break;
        case 4:
            tx_message[0] = Bat[Latch_Number - 1].Dock_Location;
            tx_message[1] = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4) + 1;
            break;
        default:
            break;
    }
    if (HAL_CAN_AddTxMessage(&hcan2_t, &CAN2_TxHeader_t, tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }     
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/