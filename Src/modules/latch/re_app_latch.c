/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_latch.c
  * Origin Date           :   05/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/
#include "latch/re_app_latch.h"

uint8_t Bifrost_Coloum = 'A';
bool Unlocklatch1 = false, Unlocklatch2 = false, Unlocklatch3 = false, Unlocklatch4 = false;
bool Latch1_Status, Latch2_Status, Latch3_Status, Latch4_Status; 

RE_StatusTypeDef RE_Open_DockLatch(void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, (GPIO_PinState)Unlocklatch1);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, (GPIO_PinState)Unlocklatch2);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState)Unlocklatch3);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState)Unlocklatch4);
    return RE_OK;
}

RE_StatusTypeDef RE_Read_Initial_DockStatus(void)
{
    Bat[0].Dock_Status = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, (GPIO_PinState)!Bat[0].Dock_Status);
    Bat[1].Dock_Status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, (GPIO_PinState)!Bat[1].Dock_Status);
    Bat[2].Dock_Status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_4, (GPIO_PinState)!Bat[2].Dock_Status);
    Bat[3].Dock_Status = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, (GPIO_PinState)!Bat[3].Dock_Status);
    if(Bat[0].Dock_Status && Bat[1].Dock_Status && Bat[2].Dock_Status && Bat[3].Dock_Status)
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
    }
    else
    {
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/