/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_nvs.c
  * Origin Date           :   15/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, SEP 2020
  *****************************************************************************
  */

/* Includes */
#include "nvs/re_app_nvs.h"

#define EEPROM_ADDRESS 0xA0

SystemConfig_t SystemConfig;
uint8_t * AddressOfStruct = (uint8_t*)(&SystemConfig);
static uint8_t SysConfig_Buffer[sizeof(SystemConfig_t)];

RE_StatusTypeDef RE_WriteDatatoNVS(void)
{
    if(HAL_I2C_Mem_Write (&hi2c2_t, EEPROM_ADDRESS,  0, 0xFFFF, AddressOfStruct, sizeof(SystemConfig) - 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_ReadDatafromNVS(void)
{
    HAL_Delay(100);
    uint16_t LenOfStruct = sizeof(SystemConfig);
    uint8_t CanId_buffer[4];
    if(HAL_I2C_Mem_Read (&hi2c2_t, EEPROM_ADDRESS, 0, 0xFFFF, SysConfig_Buffer, LenOfStruct - 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    SelfCanID   = (SysConfig_Buffer[0] << 24) | (SysConfig_Buffer[1] << 16) | (SysConfig_Buffer[2] << 8) |\
                   SysConfig_Buffer[3];
    SystemConfig.PhyID[0]         = SysConfig_Buffer[4];
    SystemConfig.PhyID[1]         = SysConfig_Buffer[5];
    SystemConfig.PhyID[2]         = SysConfig_Buffer[6];
    SystemConfig.PhyID[3]         = SysConfig_Buffer[7];
    SystemConfig.PhyID[4]         = SysConfig_Buffer[8];
    SystemConfig.PhyID[5]         = SysConfig_Buffer[9];
    SystemConfig.PhyID[6]         = SysConfig_Buffer[10];
    SystemConfig.latch_status     = SysConfig_Buffer[11];
    SystemConfig.FW_Version       = SysConfig_Buffer[12];
    SystemConfig.error_code       = SysConfig_Buffer[13]; 
    SystemConfig.Dock1_Location   = SysConfig_Buffer[14];
    SystemConfig.Dock2_Location   = SysConfig_Buffer[15];
    SystemConfig.Dock3_Location   = SysConfig_Buffer[16];
    SystemConfig.Dock4_Location   = SysConfig_Buffer[17];
    Bat[0].Dock_Location          = (int)SystemConfig.Dock1_Location;
    Bat[1].Dock_Location          = (int)SystemConfig.Dock2_Location;
    Bat[2].Dock_Location          = (int)SystemConfig.Dock3_Location;
    Bat[3].Dock_Location          = (int)SystemConfig.Dock4_Location;
    Bat[0].CanId                  = ((SysConfig_Buffer[18]) << 24) | ((SysConfig_Buffer[19]) << 16) |\
                                    ((SysConfig_Buffer[20]) << 8) | (SysConfig_Buffer[21]);
    Bat[1].CanId                  = ((SysConfig_Buffer[22]) << 24) | ((SysConfig_Buffer[23]) << 16) |\
                                    ((SysConfig_Buffer[24]) << 8) | (SysConfig_Buffer[25]);  
    Bat[2].CanId                  = ((SysConfig_Buffer[26]) << 24) | ((SysConfig_Buffer[27]) << 16) |\
                                    ((SysConfig_Buffer[28]) << 8) | (SysConfig_Buffer[29]); 
    HAL_Delay(100);
    if(HAL_I2C_Mem_Read (&hi2c2_t, EEPROM_ADDRESS, 32, 0xFFFF, CanId_buffer, 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }  
    Bat[3].CanId = ((CanId_buffer[0]) << 24) | ((CanId_buffer[1]) << 16) | ((CanId_buffer[2]) << 8) | (CanId_buffer[3]);
    return RE_OK;
}

RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data)
{
    if(HAL_I2C_Mem_Write (&hi2c2_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, &Data, 1, 200) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Write_DwordToNVS(uint16_t MemAddr, uint8_t * Data)
{
    if(HAL_I2C_Mem_Write (&hi2c2_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, Data, 4, 200) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}

RE_StatusTypeDef RE_Read_DwordFromNVS(uint16_t MemAddr, uint8_t Bat_Num)
{
    uint8_t Data_Buffer[4];
    HAL_Delay(100);
    if(HAL_I2C_Mem_Read (&hi2c2_t, EEPROM_ADDRESS, MemAddr, 0xFFFF, Data_Buffer, 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }   
    Bat[Bat_Num].CanId = ((Data_Buffer[0]) << 24) | ((Data_Buffer[1]) << 16) | ((Data_Buffer[2]) << 8) | (Data_Buffer[3]);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/