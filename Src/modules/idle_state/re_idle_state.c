/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_idle_state.c
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/
#include "idle_state/re_idle_state.h"

uint8_t * AllPacketDataBuffer_Address = AllPacketDataBuffer;
static uint8_t NoOfCanMsgsTransmitted;
uint8_t bat_num_count;
static bool Req_capturedData_Flag;
uint8_t ack_latch_ctrl[8];
uint8_t dlc;

RE_StatusTypeDef RE_Idle_State_Handler(void)
{
    if(Capture_BatData_Flag == true)
    {
        RE_Capture_Data();   
        Req_capturedData_Flag = true;
        Capture_BatData_Flag = false;
    }
    if(Tx_Ids_Flag == true)
    {
        RE_Transmit_RegistrationData();
        Tx_Ids_Flag = false;
    }
    if(t50ms_Flag == true)
    {
        t50ms_Flag = false;
        if(Bat_Registered_Flag == false)
        {
            RE_Tx_Bat_Not_Registered(bat_num);
            Bat_Registered_Flag = true;
        }
        if(Req_Bat1_Id_Flag == true)
        {
            RE_RequestPackId(1);
            Req_Bat1_Id_Flag = false;
        }
        if(Req_Bat2_Id_Flag == true)
        {
            RE_RequestPackId(2);
            Req_Bat2_Id_Flag = false;
        }
        if(Req_Bat3_Id_Flag == true)
        {
            RE_RequestPackId(3);
            Req_Bat3_Id_Flag = false;
        }
        if(Req_Bat4_Id_Flag == true)
        {
            RE_RequestPackId(4);
            Req_Bat4_Id_Flag = false;
        }
        if(Latch1_Opened == true)
        {
            RE_Tx_Latch_Open_Detected(1);
            Latch1_Opened = false;
        }
        if(Latch2_Opened == true)
        {
            RE_Tx_Latch_Open_Detected(2);
            Latch2_Opened = false;
        }
        if(Latch3_Opened == true)
        {
            RE_Tx_Latch_Open_Detected(3);
            Latch3_Opened = false;
        }
        if(Latch4_Opened == true)
        {
            RE_Tx_Latch_Open_Detected(4);
            Latch4_Opened = false;
        }
        if(Req_capturedData_Flag == true)
        {
            if(Bat_Data_Rx_Flag == true)
            {
                BataData_Len = BataData_Len + 36;
            }
            if(bat_num_count <= 3)
            {
                Bat_Data_Rx_Flag = true;
                RE_Req_CapturedData(Bat[bat_num_count].CanId);
            }
            bat_num_count++;
            if(bat_num_count > 3)
            {
                Bat_Data_Rx_Flag = false;
                bat_num_count = 0; 
                RE_CreateBatData_MegaPacket();
            }
        }
    }
    if(CAN_tx_bat_data_Flag == true)
    {
        if(t1ms_Flag == true)
        {
            t1ms_Flag = false;
            if(NoOfCanMsgsTransmitted % 7 == 0)
            {
                memcpy(bat_data_buffer, (uint8_t *)AllPacketDataBuffer_Address, 8);
                RE_Transmit_StartOfData();
                AllPacketDataBuffer_Address = AllPacketDataBuffer_Address + 8;
                NoOfCanMsgsTransmitted++;
            }
            else
            {
                memcpy(bat_data_buffer, (uint8_t *)AllPacketDataBuffer_Address, 6);
                RE_Transmit_BatteryData();
//                HAL_Delay(1);
                AllPacketDataBuffer_Address = AllPacketDataBuffer_Address + 6;
                NoOfCanMsgsTransmitted++;
            }
        }
        if(NoOfCanMsgsTransmitted == 28)
        {
            CAN_tx_bat_data_Flag = false;
            NoOfCanMsgsTransmitted = 0;
            AllPacketDataBuffer_Address = AllPacketDataBuffer;
            memset(AllPacketDataBuffer, 0, sizeof(AllPacketDataBuffer));
        }
    }
    if (UnlockLatchProcessStart == true)
    {
        if(HAL_TIM_Base_Stop_IT(&htim3_t) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);        
        for (uint8_t i = 0; i <= 7; i+= 2)
        {
            if(ack_latch_ctrl[i] != 0)
            {
                if (i == 0) 
                {
                    ack_latch_ctrl[i+1] = Bat[latch_num[0]-1].Dock_Status + 1;
                }
                else
                {
                    ack_latch_ctrl[i+1] = Bat[latch_num[(i/2)]].Dock_Status + 1;
                }
            }
        }
        RE_Tx_LatchAck(dlc-2, ack_latch_ctrl);
        UnlockLatchProcessStart = false;
        dlc = 0;
    }
    if(t500ms_Flag == true)
    {
        t500ms_Flag = false;
        if(Unlocklatch1 == true)
        {
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
            ack_latch_ctrl[dlc] = Bat[0].Dock_Location;
            Unlocklatch1 = false;
        }
        else if(Unlocklatch2 == true)
        {
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
            ack_latch_ctrl[dlc] = Bat[1].Dock_Location;
            Unlocklatch2 = false;
        }
        else if(Unlocklatch3 == true)
        {
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
            ack_latch_ctrl[dlc] = Bat[2].Dock_Location;
            Unlocklatch3 = false;
        }
        else if(Unlocklatch4 == true)
        {
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
            ack_latch_ctrl[dlc] = Bat[3].Dock_Location;
            Unlocklatch4 = false;
        }
        else
        {
            UnlockLatchProcessStart = true;
        }      
        dlc += 2;
    }
    if(charger1Status_Flag == true)
    {
        RE_Tx_Charger_Ack(1);
        charger1Status_Flag = false;
    }
    if(charger2Status_Flag == true)
    {
        RE_Tx_Charger_Ack(2);
        charger2Status_Flag = false;
    }
    if(charger3Status_Flag == true)
    {
        RE_Tx_Charger_Ack(3);
        charger3Status_Flag = false;
    }
    if(charger4Status_Flag == true)
    {
        RE_Tx_Charger_Ack(4);
        charger4Status_Flag = false;
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/