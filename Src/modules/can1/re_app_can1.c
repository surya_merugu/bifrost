/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_can1.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */
/* Includes*/

#include "can1/re_app_can1.h"

#define CAPTURE_INFO_CMD    0x5E
#define GET_CAPTURED_INFO   0x5F
#define GET_BAT_ID_CMD      0x09

uint8_t AllPacketDataBuffer[250];
uint8_t BatData_Buffer[200];
uint8_t BataData_Len = 0;
uint32_t SelfCanID;
BatteryData_t Bat[4];
bool Bat_Data_Rx_Flag;
bool Bat_Registered_Flag;

RE_StatusTypeDef RE_CreateBatData_MegaPacket(void)
{   
    for(uint8_t i = 0; i < sizeof(AllPacketDataBuffer); i++)
    {
      AllPacketDataBuffer[i] = 0x0;
    }
    HAL_Delay(1);
    AllPacketDataBuffer[0] = (Bat[0].CanId & 0xFF);
    AllPacketDataBuffer[1] = ((Bat[0].CanId >> 8) & 0xFF);
    AllPacketDataBuffer[2] = ((Bat[0].CanId >> 16) & 0xFF);
    AllPacketDataBuffer[3] = ((Bat[0].CanId >> 24) & 0xFF);
    AllPacketDataBuffer[4] = 1;     // start of communication
    AllPacketDataBuffer[5] = Bat[0].Dock_Location;     // Dock Location
    Bat[0].Dock_Status     = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
    AllPacketDataBuffer[6] = Bat[0].Dock_Status + 1;   
    AllPacketDataBuffer[7] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_5) + 1;
    HAL_Delay(1);
    memcpy(AllPacketDataBuffer + 8, BatData_Buffer, 36);
    HAL_Delay(1);
    AllPacketDataBuffer[44] = (Bat[1].CanId & 0xFF);
    AllPacketDataBuffer[45] = ((Bat[1].CanId >> 8) & 0xFF);
    AllPacketDataBuffer[46] = ((Bat[1].CanId >> 16) & 0xFF);
    AllPacketDataBuffer[47] = ((Bat[1].CanId >> 24) & 0xFF);
    AllPacketDataBuffer[48] = 1;     // start of communication  
    AllPacketDataBuffer[49] = Bat[1].Dock_Location;     // Dock Location
    Bat[1].Dock_Status      = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
    AllPacketDataBuffer[50] = Bat[1].Dock_Status + 1;   
    AllPacketDataBuffer[51] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_6) + 1;
    HAL_Delay(1);
    memcpy(AllPacketDataBuffer + 52, BatData_Buffer + 36, 36);
    HAL_Delay(1);
    AllPacketDataBuffer[88] = (Bat[2].CanId & 0xFF);
    AllPacketDataBuffer[89] = ((Bat[2].CanId >> 8) & 0xFF);
    AllPacketDataBuffer[90] = ((Bat[2].CanId >> 16) & 0xFF);
    AllPacketDataBuffer[91] = ((Bat[2].CanId >> 24) & 0xFF);
    AllPacketDataBuffer[92] = 1;     // start of communication
    AllPacketDataBuffer[93] = Bat[2].Dock_Location;     // Dock Location
    Bat[2].Dock_Status      = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
    AllPacketDataBuffer[94] = Bat[2].Dock_Status + 1;   
    AllPacketDataBuffer[95] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_7) + 1;
    HAL_Delay(1);
    memcpy(AllPacketDataBuffer + 96, BatData_Buffer + 72, 36);
    HAL_Delay(1);
    AllPacketDataBuffer[132] = (Bat[3].CanId & 0xFF);
    AllPacketDataBuffer[133] = ((Bat[3].CanId >> 8) & 0xFF);
    AllPacketDataBuffer[134] = ((Bat[3].CanId >> 16) & 0xFF);
    AllPacketDataBuffer[135] = ((Bat[3].CanId >> 24) & 0xFF);
    AllPacketDataBuffer[136] = 1;     // start of communication
    AllPacketDataBuffer[137] = Bat[3].Dock_Location;     // Dock Location
    Bat[3].Dock_Status       = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6);
    AllPacketDataBuffer[138] = Bat[3].Dock_Status + 1;   
    AllPacketDataBuffer[139] = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8) + 1;
    HAL_Delay(1);
    memcpy(AllPacketDataBuffer + 140, BatData_Buffer + 108, 36);  
    HAL_Delay(1);
    for(uint8_t i = 0; i < sizeof(BatData_Buffer); i++)
    {
      BatData_Buffer[i] = 0x0;
    }
    BataData_Len = 0;
    return RE_OK;
}

RE_StatusTypeDef RE_Capture_Data(void)
{
    uint32_t TxMailbox;
    uint8_t tx_msg;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xC9;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA; 
    tx_msg                = CAPTURE_INFO_CMD;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_msg,  &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }       
    return RE_OK;
}

RE_StatusTypeDef RE_Req_CapturedData(uint32_t batId)
{
    uint32_t TxMailbox;
    uint8_t tx_msg[8];
    tx_msg[0] = GET_CAPTURED_INFO;
    tx_msg[1] = (batId & 0xFF);
    tx_msg[2] = ((batId >> 8)  & 0xFF);
    tx_msg[3] = ((batId >> 16) & 0xFF);
    tx_msg[4] = ((batId >> 24) & 0xFF);
    CAN1_TxHeader_t.DLC   = 5;
    CAN1_TxHeader_t.ExtId = 0xC9;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Req_BatIds(void)
{
    uint32_t TxMailbox;
    uint8_t tx_message;
    CAN1_TxHeader_t.DLC   = 1;
    CAN1_TxHeader_t.ExtId = 0xC9;
    tx_message            = GET_BAT_ID_CMD;
    CAN1_TxHeader_t.IDE   = CAN_ID_EXT;
    CAN1_TxHeader_t.RTR   = CAN_RTR_DATA;
    if (HAL_CAN_AddTxMessage(&hcan1_t, &CAN1_TxHeader_t, &tx_message, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;  
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/