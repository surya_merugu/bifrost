/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   main.c
  * Origin Date           :   25/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */
/* Includes*/
#include "main/main.h"

uint32_t pclk, hclk;

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    HAL_Init();
    RE_SystemClock_Config();
    pclk = HAL_RCC_GetPCLK1Freq();
    hclk = HAL_RCC_GetHCLKFreq();
    RE_UART2_Init();
    RE_NVS_Init();
    RE_ADC1_Init();
    RE_Latch_Init();
    RE_Read_Initial_DockStatus();
    RE_Location_GpioInit();
    RE_Charger_GpioInit();
    RE_CAN1_Init();
    RE_CAN1_Filter_Config();
    RE_CAN2_Init();
    RE_CAN2_Filter_Config();
    RE_CAN1_Start_Interrupt();
    RE_CAN2_Start_Interrupt();
    RE_TIMER2_Init();
    RE_TIMER3_Init();
    RE_TIMER5_Init();
    RE_ReadDatafromNVS();
    Bat_Registered_Flag = true;
    /**Debug Only*/
//    dummy_message();
    /***/
    if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if(HAL_TIM_Base_Start_IT(&htim5_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    while (1)
    {
        RE_Idle_State_Handler();
    }
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/
