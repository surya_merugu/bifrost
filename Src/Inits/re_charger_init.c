/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_charger_init.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_charger_init.h"

bool charger1Status_Flag, charger2Status_Flag, charger3Status_Flag, charger4Status_Flag;

/**
 * @brief  Latch GPIO Initialization
 * This function configures the GPIO pins used by  Latch
 * @param None
 * @retval Exit status
 */
RE_StatusTypeDef RE_Charger_GpioInit (void)
{
    GPIO_InitTypeDef ChargerPin;
    __HAL_RCC_GPIOD_CLK_ENABLE();

     ChargerPin.Pin               = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8;
     ChargerPin.Mode              = GPIO_MODE_OUTPUT_PP;
     ChargerPin.Speed             = GPIO_SPEED_FAST;
     ChargerPin.Pull              = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOD, &ChargerPin);
     return RE_OK;
}

