/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_location_init.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_location_init.h"

/**
 * @brief  Latch GPIO Initialization
 * This function configures the GPIO pins used by  Latch
 * @param None
 * @retval Exit status
 */
RE_StatusTypeDef RE_Location_GpioInit (void)
{
    GPIO_InitTypeDef LocationPin, ExtraGpio;
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

     LocationPin.Pin               = GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4;
     LocationPin.Mode              = GPIO_MODE_OUTPUT_PP;
     LocationPin.Speed             = GPIO_SPEED_FAST;
     LocationPin.Pull              = GPIO_NOPULL;
     HAL_GPIO_Init(GPIOD, &LocationPin);
     
     ExtraGpio.Pin               = GPIO_PIN_0;
     ExtraGpio.Mode              = GPIO_MODE_OUTPUT_PP;
     ExtraGpio.Speed             = GPIO_SPEED_FAST;
     ExtraGpio.Pull              = GPIO_NOPULL;
     HAL_GPIO_Init(GPIOA, &ExtraGpio);     
     return RE_OK;
}

