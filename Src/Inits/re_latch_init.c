/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_latch_init.c
  * Origin Date           :   25/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_latch_init.h"

/**
 * @brief Dock Latch GPIO Initialization
 * This function configures the GPIO pins used by Dock Latch
 * @param None
 * @retval Exit status
 */
RE_StatusTypeDef RE_Latch_Init(void)
{
    GPIO_InitTypeDef LatchCtrl, Latchstatus, Debug_Led, Dock_Status_Led;
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    
     LatchCtrl.Pin               = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8;
     LatchCtrl.Mode              = GPIO_MODE_OUTPUT_PP;
     LatchCtrl.Speed             = GPIO_SPEED_FAST;
     LatchCtrl.Pull              = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &LatchCtrl);
     
     Latchstatus.Pin         = GPIO_PIN_1 | GPIO_PIN_4;
     Latchstatus.Mode        = GPIO_MODE_IT_RISING_FALLING;
     Latchstatus.Speed       = GPIO_SPEED_FAST;
     Latchstatus.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &Latchstatus);

     Latchstatus.Pin         = GPIO_PIN_3 | GPIO_PIN_6;
     Latchstatus.Mode        = GPIO_MODE_IT_RISING_FALLING;
     Latchstatus.Speed       = GPIO_SPEED_FAST;
     Latchstatus.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOC, &Latchstatus);
     
     Dock_Status_Led.Pin         = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5;
     Dock_Status_Led.Mode        = GPIO_MODE_OUTPUT_PP;
     Dock_Status_Led.Speed       = GPIO_SPEED_FAST;
     Dock_Status_Led.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOC, &Dock_Status_Led);
     
     Debug_Led.Pin         = GPIO_PIN_12;
     Debug_Led.Mode        = GPIO_MODE_OUTPUT_PP;
     Debug_Led.Speed       = GPIO_SPEED_FAST;
     Debug_Led.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOD, &Debug_Led);  
     
     HAL_NVIC_SetPriority(EXTI1_IRQn,0,0);
     HAL_NVIC_EnableIRQ(EXTI1_IRQn);

     HAL_NVIC_SetPriority(EXTI3_IRQn,0,0);
     HAL_NVIC_EnableIRQ(EXTI3_IRQn);
     
     HAL_NVIC_SetPriority(EXTI4_IRQn,0,0);
     HAL_NVIC_EnableIRQ(EXTI4_IRQn);
     
     HAL_NVIC_SetPriority(EXTI9_5_IRQn,0,0);
     HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
     return RE_OK;
}

