/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_adc_init.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Includes */
#include "re_adc_init.h"

ADC_HandleTypeDef hadc1_t;

static RE_StatusTypeDef RE_ADC_GpioInit(ADC_HandleTypeDef *hadc);

/**
 * @brief Timer Base Init
 * This function configures clock and set priority for the timer
 * @param  htim3_t: timer handle typedef
 * @retval Exit status
 */
static RE_StatusTypeDef RE_ADC_GpioInit(ADC_HandleTypeDef *hadc)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(hadc -> Instance == ADC1)
    {
        __HAL_RCC_ADC1_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**ADC1 GPIO Configuration    
        PA4     ------> ADC1_IN4
        PA5     ------> ADC1_IN5
        */
        GPIO_InitStruct.Pin     =  GPIO_PIN_4 | GPIO_PIN_5;
        GPIO_InitStruct.Mode    = GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull    = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(ADC_IRQn);
    }
    return RE_OK;
}

/**
 * @brief ADC Initialization
 * This function configures the ADC
 * @param  void
 * @retval Exit status
 */
RE_StatusTypeDef RE_ADC1_Init (void)
{
    ADC_ChannelConfTypeDef sConfig = {0};

    /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
    hadc1_t.Instance                      = ADC1;
    hadc1_t.Init.ClockPrescaler           = ADC_CLOCK_SYNC_PCLK_DIV8;
    hadc1_t.Init.Resolution               = ADC_RESOLUTION_12B;
    hadc1_t.Init.ScanConvMode             = ENABLE;
    hadc1_t.Init.ContinuousConvMode       = ENABLE;
    hadc1_t.Init.DiscontinuousConvMode    = DISABLE;
    hadc1_t.Init.ExternalTrigConvEdge     = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc1_t.Init.ExternalTrigConv         = ADC_SOFTWARE_START;
    hadc1_t.Init.DataAlign                = ADC_DATAALIGN_RIGHT;
    hadc1_t.Init.NbrOfConversion          = 1;
    hadc1_t.Init.DMAContinuousRequests    = ENABLE;
    hadc1_t.Init.EOCSelection             = ADC_EOC_SINGLE_CONV;
    if (RE_ADC_GpioInit(&hadc1_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if (HAL_ADC_Init(&hadc1_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
    sConfig.Channel       = ADC_CHANNEL_4;
    sConfig.Rank          = 5;
    if (HAL_ADC_ConfigChannel(&hadc1_t, &sConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
    sConfig.Channel       = ADC_CHANNEL_5;
    sConfig.Rank          = 6;
    if (HAL_ADC_ConfigChannel(&hadc1_t, &sConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/
