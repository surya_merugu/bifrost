/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_idle_state.h
  * Origin Date           :   04/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_IDLE_STATE_H
#define __RE_IDLE_STATE_H

#include "re_std_def.h"
#include "re_can_int_callback.h"
#include "re_timer_int_callback.h"
#include "re_can_init.h"
#include "latch/re_app_latch.h"
#include "re_gpio_int_callback.h"
#include "re_charger_init.h"
#include "location/re_app_location.h" 

extern bool UnlockLatchProcessStart;
extern uint8_t latch_num[4];
RE_StatusTypeDef RE_Idle_State_Handler(void);

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/