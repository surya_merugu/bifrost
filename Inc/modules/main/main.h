/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   main.h
  * Origin Date           :   25/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H

#include "main/main.h"
#include "re_sys_clk_config.h"
#include "stm32f4xx_it.h"
#include "re_std_def.h"
#include "re_can_init.h"
#include "re_uart_init.h"
#include "re_latch_init.h"
#include "re_i2c_init.h"
#include "re_adc_init.h"
#include "re_location_init.h"
#include "re_charger_init.h"
#include "can1/re_app_can1.h"
#include "can2/re_app_can2.h"
#include "re_timer_init.h"
#include "re_timer_int_callback.h"
#include "idle_state/re_idle_state.h"
#include "latch/re_app_latch.h"
#include "nvs/re_app_nvs.h"

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/