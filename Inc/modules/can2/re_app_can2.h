/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_can2.h
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

#ifndef __RE_APP_CAN2_H
#define __RE_APP_CAN2_H

#include "re_can_init.h"
#include "nvs/re_app_nvs.h"

extern uint8_t bat_num;
extern uint8_t bat_data_buffer[8];

RE_StatusTypeDef RE_Transmit_SelfIDs(void);
RE_StatusTypeDef RE_Transmit_BatteryData(void);
RE_StatusTypeDef RE_Transmit_StartOfData(void);
RE_StatusTypeDef RE_Transmit_RegistrationData(void);
RE_StatusTypeDef RE_Tx_LatchAck(uint8_t len, uint8_t *pData);
RE_StatusTypeDef RE_Tx_Charger_Ack(uint8_t Charger_Location);
RE_StatusTypeDef RE_Tx_Bat_Not_Registered(uint8_t bat_num);
RE_StatusTypeDef RE_Tx_Latch_Open_Detected(uint8_t Latch_Number);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/