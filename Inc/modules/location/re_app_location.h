/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_location.h
  * Origin Date           :   24/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_APP_LOCATION_H
#define __RE_APP_LOCATION_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "can2/re_app_can2.h"
#include "can1/re_app_can1.h"

RE_StatusTypeDef RE_TurnOff_Location(void);
RE_StatusTypeDef RE_TurnOn_Dock1Location(void);
RE_StatusTypeDef RE_TurnOn_Dock2Location(void);
RE_StatusTypeDef RE_TurnOn_Dock3Location(void);
RE_StatusTypeDef RE_TurnOn_Dock4Location(void);
RE_StatusTypeDef RE_RequestPackId(uint8_t Location);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/