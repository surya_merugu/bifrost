/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_nvs.h
  * Origin Date           :   15/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, SEP 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_NVS_H
#define _RE_APP_NVS_H

#include "stm32f4xx_hal.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "re_i2c_init.h"
#include "can1/re_app_can1.h"

typedef struct __attribute__((packed))
{
    uint8_t CanID[4];
    uint8_t PhyID[7];
    uint8_t latch_status;
    uint8_t FW_Version;
    uint8_t error_code;
    uint8_t Dock1_Location;
    uint8_t Dock2_Location;
    uint8_t Dock3_Location;
    uint8_t Dock4_Location;
    uint8_t Bat0_CanId[4];
    uint8_t Bat1_CanId[4];
    uint8_t Bat2_CanId[4];
    uint8_t Bat3_CanId[4];
}SystemConfig_t;

extern SystemConfig_t SystemConfig;

RE_StatusTypeDef RE_WriteDatatoNVS(void);
RE_StatusTypeDef RE_ReadDatafromNVS(void);
RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data);
RE_StatusTypeDef RE_Write_DwordToNVS(uint16_t MemAddr, uint8_t * Data);
RE_StatusTypeDef RE_Read_DwordFromNVS(uint16_t MemAddr, uint8_t Bat_Num);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/