/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_can1.h
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

#ifndef __RE_APP_CAN1_H
#define __RE_APP_CAN1_H

#include "string.h"
#include "re_can_init.h"
#include "can2/re_app_can2.h"
#include "stdio.h"
#include "string.h"
#include "stdbool.h"
#include "latch/re_app_latch.h"

typedef struct __attribute__((packed))
{
    uint8_t Dock_Location;
    bool Dock_Status;
    uint8_t PhyId[7];
    uint32_t CanId;
}BatteryData_t;

extern uint8_t AllPacketDataBuffer[250];
extern uint32_t SelfCanID;
extern BatteryData_t Bat[4];
extern uint8_t BatData_Buffer[200];
extern uint8_t BataData_Len;
extern bool Bat_Data_Rx_Flag;
extern bool Bat_Registered_Flag;

RE_StatusTypeDef RE_CreateBatData_MegaPacket(void);
RE_StatusTypeDef RE_Capture_Data(void);
RE_StatusTypeDef RE_Req_CapturedData(uint32_t CanId);
RE_StatusTypeDef RE_Req_BatIds(void);

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/