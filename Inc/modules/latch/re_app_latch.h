/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_app_latch.h
  * Origin Date           :   05/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_APP_LATCH_H
#define __RE_APP_LATCH_H

#include "re_std_def.h"
#include "re_can_int_callback.h"
#include "re_timer_int_callback.h"
#include "re_can_init.h"
#include "stdbool.h"

extern uint8_t Bifrost_Coloum;
extern bool Unlocklatch1, Unlocklatch2, Unlocklatch3, Unlocklatch4;
extern bool Latch1_Status, Latch2_Status, Latch3_Status, Latch4_Status;

RE_StatusTypeDef RE_Open_DockLatch(void);
RE_StatusTypeDef RE_Read_Initial_DockStatus(void);

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/