/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_i2c_init.h
  * Origin Date           :   25/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_I2C_INIT_H
#define _RE_I2C_INIT_H

/* Includes */
#include "re_std_def.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_i2c.h"

extern I2C_HandleTypeDef hi2c2_t;

/* Exported API */
RE_StatusTypeDef RE_NVS_Init(void);

#endif
/**************************** END OF FILE *************************************/
