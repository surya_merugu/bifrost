/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_charger_init.h
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_CHARGER_INIT_H
#define _RE_CHARGER_INIT_H

/* Includes */
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "stdbool.h"

extern bool charger1Status_Flag, charger2Status_Flag, charger3Status_Flag, charger4Status_Flag;
/* Exported API's */

RE_StatusTypeDef RE_Charger_GpioInit(void);

#endif
/**************************** END OF FILE *************************************/
