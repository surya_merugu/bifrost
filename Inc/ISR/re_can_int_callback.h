/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   27/07/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUL 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_CAN_INT_CALLBACK_H
#define _RE_CAN_INT_CALLBACK_H

/* Includes */
#include "main/main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"
#include "re_std_def.h"
#include "re_can_init.h"
#include "can1/re_app_can1.h"
#include "can2/re_app_can2.h"
#include "stdbool.h"
#include "latch/re_app_latch.h"
#include "re_charger_init.h"

extern bool CAN_tx_bat_data_Flag;
extern bool OpenDockLatch_Flag;
extern bool Capture_BatData_Flag;
extern bool TurnONCharger_Flag;
extern bool Tx_Ids_Flag;

#endif
/***************************** END OF FILE ************************************/