/**
  *****************************************************************************
  * Title                 :   Bifrost
  * Filename              :   re_gpio_int_callback.h
  * Origin Date           :   06/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/******** Define to prevent recursive inclusion*****************************/
#ifndef _RE_GPIO_INT_CALLBACK_H
#define _RE_GPIO_INT_CALLBACK_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_it.h"
#include "re_std_def.h"
#include "latch/re_app_latch.h"

extern bool Req_Bat1_Id_Flag, Req_Bat2_Id_Flag, Req_Bat3_Id_Flag, Req_Bat4_Id_Flag;
extern bool Latch1_Opened, Latch2_Opened, Latch3_Opened, Latch4_Opened;

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/
